# mod_g723_1
freeswitch G723 codec mod_g723_1.so

# usage
put this directory into ${root-dir}/freeswitch-1.10.10/src/mod/codecs/    

```
cd freeswitch-1.10.10/src/mod/codecs/
git clone https://gitee.com/jetydu/mod_g723.git
cd mod_g723
make install
```

this will compile & install mod_g723_1.so auto

mod_g723_1 support g723 codec with L16

